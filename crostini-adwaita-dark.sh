#!/usr/bin/env bash


# Check if run as root.
if [ "$EUID" == "0" ];
then
	echo This script cannot be run as root.
	exit 1 # script was run as root; exit with code 1
fi


# Install Dependencies.
echo -e "\nInstalling dependencies..."

sudo apt update -y
sudo apt install -y libglib2.0-bin \
		    gnome-themes-extra \
		    papirus-icon-theme

echo -e "Finished installing dependencies!\n"


# Set GTK theme.
if ! [ `gsettings get org.gnome.desktop.interface gtk-theme` == "'Adwaita-dark'" ];
then
	gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark
	echo Set GTK theme to \"Adwaita-dark\"!
else
	echo GTK theme is already set to \"Adwaita-dark\"!
fi


# Set icons.
if ! [ `gsettings get org.gnome.desktop.interface icon-theme` == "'Papirus-Dark'" ];
then
	gsettings set org.gnome.desktop.interface icon-theme Papirus-Dark
	echo Set icon theme to \"Papirus-Dark\"!
else
	echo Icon theme is already set to \"Papirus-Dark\"!
fi

echo -e "\nDone!\n"
