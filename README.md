# Crostini Adwaita-dark
This simple script will install Adwaita-dark along with other GTK themes and the Papirus icon theme and use `gsettings` to apply them.
`gsettings` will be installed if needed as well.
It is intended to be used within a Crostini environment (Chromebook > Settings > Developers > Linux development environment).
While the script probably works elsewhere, it is not recommended that you run it in any other environment.

### Usage/Installation
To run the script, simply copy and paste the following into the Linux terminal on your Chromebook and press `ENTER`:

```
bash -c "$(curl -fsSL https://gitlab.com/nickgirga/crostini-adwaita-dark/-/raw/main/crostini-adwaita-dark.sh)"
```

***Done!*** Your GTK apps should now look as they do in most other environments.


Note that this command assumes that you have `curl` installed (and you probably do already do have it).
If `curl` is not installed already, you can install it by running the following commands:

```
sudo apt update && sudo apt install curl
```

### Removal
To undo the changes that the script did, you must first reset the theme settings.
You can do this with the following two commands:

```
gsettings set org.gnome.desktop.interface gtk-theme CrosAdapta
```

```
gsettings set org.gnome.desktop.interface icon-theme CrosAdapta
```

After the theme settings have been reset, you can uninstall the packages containing the GTK theme and the icon theme with the following commands:

```
sudo apt remove gnome-themes-extra papirus-icon-theme && sudo apt autoremove
```

***Done!*** Your GTK apps should now look as they did before you ran the script.
